var myApp = angular.module('myApp', []);

myApp.directive('inputDir', ['colorService', function (colorService) {
    return {
        restrict: 'E',
        template: `
     <div>
     <p>What is you favorite color?</p>
     <input type="text" ng-model="colorInserted"></input>
     <button type="submit" ng-click="onSubmit()">Submit</button>
     </div>`,
        link: function (scope) {
            scope.colorService = colorService;
            scope.onSubmit = function () {
                scope.colorService.color = scope.colorInserted;
                scope.colorInserted = "";
            }
        }
    }
}
]);

myApp.directive('bannerDir', ['colorService', function (colorService) {
    return {
        restrict: 'E',
        template: `<div>
     <p>The color of the day is {{colorService.color}}!</p>
     </div>`,
        link: function (scope) {
            scope.colorService = colorService;
        }
    }
}
]);

myApp.factory('colorService', function () {
    return {
        color: "green"
    }

});

